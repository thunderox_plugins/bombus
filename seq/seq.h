
#ifndef SEQ_H
#define SEQ_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;


struct seq_note
{
	int channel;
	int note;
	int volume;
	int offset;
};	

struct seq_notes
{
	vector <seq_note> notes;
};

struct seq_block
{
	string name;
	vector <seq_notes> sequence;	
};

class seq
{
	public:
	
	seq();
	~seq();
	
	int create_block(string,int);
	void add_note(string, int, int, int, int, int);
	seq_block* get_block(string name);
	
	vector <seq_block> blocks;
	
};

#endif





