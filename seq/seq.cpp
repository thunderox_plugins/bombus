
#include "seq.h"

//-----------------------------------------------------------------------------------------------------

seq::seq()
{

}

//-----------------------------------------------------------------------------------------------------

seq::~seq()
{

}

//-----------------------------------------------------------------------------------------------------

int seq::create_block(string name, int length)
{
	seq_block new_block;
	new_block.name = name;
	
	for (int x=0; x<length; x++)
	{
		seq_notes* new_notes = new seq_notes();
		new_block.sequence.push_back(*new_notes);
	}
	
	blocks.push_back(new_block);
	
	return blocks.size()-1;
}

//-----------------------------------------------------------------------------------------------------

void seq::add_note(string name, int channel, int note, int volume, int position, int offset)
{
	for (int x=0; x<blocks.size(); x++)
	{
		if (name == blocks[x].name)
		{
			seq_note new_note;
			new_note.channel = channel;
			new_note.note = note;
			new_note.volume = volume;
			new_note.offset = offset;
			if ( position >= 0 && position < blocks[x].sequence.size())
			{
				blocks[x].sequence[position].notes.push_back(new_note);
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------------

seq_block* seq::get_block(string name)
{

	seq_block* block = NULL;

	for (int x=0; x<blocks.size(); x++)
	{
		if (name == blocks[x].name)
		{
			block = &blocks[x];
		}
	}
	
	return block;
}



