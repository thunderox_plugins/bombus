
#define NANOVG_GL3_IMPLEMENTATION
#include "widget.h"

//----------------------------------------------------------------------------------------------
void widget_block::draw(NVGcontext* vg)
{
	
	// Clear Background
	
	nvgBeginPath(vg);
	nvgRect(vg, x,y,w,h);
	nvgFillColor(vg, nvgRGB(20,20,20)); 
	nvgFill(vg);

	// Draw Keyboard 
	
	int gx = x + 80;
	int gy = y + title_bar;
	int gw = w - 80;
	int gh = h - (title_bar*2);
		
	nvgBeginPath(vg);
	nvgRect(vg, x, gy, 80, gh);
	nvgFillColor(vg, nvgRGB(255,255,255)); 
	nvgFill(vg);
	
	nvgBeginPath(vg);
	nvgFillColor(vg, nvgRGB(0,0,0));
	
	int key_pattern = offset_y;
	
	for (int nt = gy+gh; nt > gy; nt -= note_height)
	{	
		if (key_pattern % 12 == 1 or key_pattern % 12 == 3
			or key_pattern % 12 == 6 or key_pattern % 12 == 8 
			or key_pattern % 12 == 10)
		{	
			nvgRect(vg, x, nt - note_height, 70, note_height);
		}
		
		if (key_pattern % 12 == 0 or key_pattern % 12 == 5) nvgRect(vg, x, nt, 80, 2);
		
		key_pattern ++;
		
		if (key_pattern > 127) break;
	}
	nvgFill(vg);
	
	
	// Draw Grid
	
	nvgBeginPath(vg);
	nvgStrokeColor(vg, nvgRGB(200,200,200));
	nvgStrokeWidth(vg,0.75);
		
	// Horizontal Lines (Note position)
	
	int bar = offset_x % 4;
	
	for (int hz = gx; hz < gx + gw; hz += note_width)
	{	

		nvgMoveTo(vg, hz, gy);
		nvgLineTo(vg, hz, gy + gh);
		
		if (bar == 0)
		{
			nvgMoveTo(vg, hz + 1,gy);
			nvgLineTo(vg, hz + 1,gy + gh);
		}
		
		bar += 1;
		if (bar > 3) bar = 0;
	}
	
	// Vertical Lines (Note and Octaves)
	
	int octave = offset_y;
	
	for (int vrt = gy + gh; vrt > gy; vrt -= note_height)
	{	
		nvgMoveTo(vg, gx, vrt);
		nvgLineTo(vg, gx+gw, vrt);
		
		if (octave % 12 == 0)
		{
			nvgMoveTo(vg, gx, vrt + 1);
			nvgLineTo(vg, gx+gw, vrt + 1);
		}
		
		octave += 1;
		if (octave > 127) break;
	}
	
	nvgStroke(vg);
	
	// Horizontal Note Position Labels
	
	nvgBeginPath(vg);
	nvgFontSize(vg, 14.0f);
	nvgFontFace(vg, "sans");
	nvgFillColor(vg, nvgRGBA(255,255,255,255));

	bar = offset_x;
	
	for (int hz = gx; hz < gx + gw; hz += note_width )
	{	
		if (bar % 4 == 0)
		{
			string bar_string = to_string(bar);
			nvgText(vg, hz-4, gh+gy+14, bar_string.c_str(),NULL);
		}
		bar ++;
	}
	nvgStroke(vg);
	
	// Draw Notes
	
	int seq_start = offset_x;
	int seq_end = offset_x + (gw / note_width);
	
	nvgBeginPath(vg);
	
	if (seq_end > block->sequence.size()-1 ) seq_end = block->sequence.size()-1;
	nvgFillColor(vg, nvgRGB(255,255,255)); 
	bool draw_notes = false;
	
	for (int sp=seq_start; sp<seq_end; sp++)
	{

		for (int np=0; np<block->sequence[sp].notes.size(); np++)
		{
		
			int nx = gx + ((sp-offset_x) * note_width); 
			int ny = gy + gh - note_height - (note_height * (block->sequence[sp].notes[np].note - offset_y));

			if (nx >= gx && nx < gx+gw && ny > gy && ny < gy+gh)
			{
				nvgRect(vg, nx+2, ny+2, note_width-4, note_height-4);
				draw_notes = true;
			}
		}
	}
	
	if (draw_notes) nvgFill(vg);
		
	// Draw Labe;

	nvgBeginPath(vg);
	nvgFontSize(vg, 14.0f);
	nvgFontFace(vg, "sans");
	nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
	if (hover) nvgFillColor(vg, nvgRGBA(255,255,255,255));
		else nvgFillColor(vg, nvgRGBA(200,200,200,255));
	nvgText(vg, x, y+14, text_top.c_str(),NULL);
	
	if (my > gy-y && my < gh+title_bar)
	{
		int mouse_over_note = (my / note_height) - 2;
		nvgBeginPath(vg);
		nvgRect(vg, x, gy + (mouse_over_note * note_height), gw + 80, note_height);
		nvgFillColor(vg, nvgRGBA(255,0,0,50)); 
		nvgFill(vg);	
		current_note = (((h-(title_bar*2)) / note_height) - mouse_over_note) + offset_y;
	}
}


//----------------------------------------------------------------------------------------------
void widget_block::left_button()
{
	value = 1 - value;
}

//----------------------------------------------------------------------------------------------
void widget_block::middle_button()
{
	if (mx > -1)
	{
		offset_x += mx - old_mx;
	
		if (offset_x < 0) offset_x = 0;
	}
	
	if (my > -1)
	{
		offset_y += my - old_my;
	
		int octave_end = 128 - ((h-(title_bar*2)) / note_height);
	
		if (offset_y < 0) offset_y = 0;
		if (offset_y > octave_end) offset_y = octave_end;
	}
}
	


