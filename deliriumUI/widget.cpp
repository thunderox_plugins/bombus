
#define NANOVG_GL3_IMPLEMENTATION
#include "widget.h"


//----------------------------------------------------------------------------------------------

widget::widget()
{
	value = 0;
	value_increment = 0.05;
	old_mx = -1;
	old_my = -1;
	title_bar = 32;
	
	note_width = 24;
	note_height = 16;
	offset_x = 0;
	offset_y = 0;
}

//----------------------------------------------------------------------------------------------
widget::~widget()
{
}

//----------------------------------------------------------------------------------------------
void widget::set_position(float _x, float _y)
{

}

//----------------------------------------------------------------------------------------------
void widget::set_size(float _w, float _h)
{

}

//----------------------------------------------------------------------------------------------
void widget::draw(NVGcontext* vg)
{


}

//----------------------------------------------------------------------------------------------
void widget::drag(float mx,float my)
{
	value = 1 - ((1.0 / h) * (my - y));	
	
	if (value < 0) value = 0;
	if (value > 1) value = 1;
}

//----------------------------------------------------------------------------------------------
void widget::value_inc()
{
	value -= value_increment;
	if (value < 0) value = 0;
}

//----------------------------------------------------------------------------------------------
void widget::value_dec()
{
	value += value_increment;
	if (value > 1) value = 1;
}

//----------------------------------------------------------------------------------------------
void widget::left_button()
{

}


//----------------------------------------------------------------------------------------------
void widget::middle_button()
{

}









