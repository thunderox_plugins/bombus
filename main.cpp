
#include <iostream>
#include "deliriumUI/deliriumUI.h"
#include "jack/jack_manager.h"
#include "seq/seq.h"

using namespace std;

int main()
{

	deliriumUI main_gui;

	int win = main_gui.create_window(0, 0, main_gui.screen_width, main_gui.screen_height, "Bombus (Digital Audio Workstation) - v 0.1 - ThunderOx Software 2021~2023");
	main_gui.set_current_window(win);
	main_gui.set_window_grid(win, 44, 32);

	main_gui.create_widget(widget_type_button, win, 1, 1, 3, 1, "FRAGGLES!");

	int block_id1 = main_gui.create_widget(widget_type_block, win, 1, 3, 40, 24, "Block One");
	
	seq nick_seq;
	
	nick_seq.create_block("Spud position 36", 128);

	nick_seq.add_note("Spud position 36", 1, 0, 100, 0, 0);	
	nick_seq.add_note("Spud position 36", 1, 12*4, 100, 0, 0);
	nick_seq.add_note("Spud position 36", 1, 12*5, 100, 4, 0);
	nick_seq.add_note("Spud position 36", 1, 12*3, 100, 0, 0);
	nick_seq.add_note("Spud position 36", 1, 48, 100, 8, 0);
	nick_seq.add_note("Spud position 36", 1, 33, 100, 16, 0);
		
	main_gui.set_block ( nick_seq.get_block("Spud position 36"), win, block_id1 );
	
	
	
	jack_manager my_jack_manager;
	my_jack_manager.set_current_manager(&my_jack_manager);
	my_jack_manager.create_client("bombus");
	my_jack_manager.create_jack_out("bombus","track-1");	
	my_jack_manager.connect_audio("bombus", ""); 	 	

	main_gui.main_loop();

	my_jack_manager.close_client("bombus"); 	

	return 0;
}
